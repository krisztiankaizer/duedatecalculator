
## Due Date Calculator

### Installation
```bash
$ git clone https://krisztiankaizer@bitbucket.org/krisztiankaizer/duedatecalculator.git <my-project-name>
$ cd <my-project-name>
```

When it's done, install the project dependencies:

```bash
$ npm install
```

### Usage

```bash
$ npm run jest
$ npm run jest:watch
$ npm run build
$ npm run build:watch
```
