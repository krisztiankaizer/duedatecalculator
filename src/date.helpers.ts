export enum Days {
  SUNDAY = 0,
  MONDAY = 1,
  TUESDAY = 2,
  WEDNESDAY = 3,
  THURSDAY = 4,
  FRIDAY = 5,
  SATURDAY = 6,
}

export const transformToTwoDigits = (valueString: any) => {
  return valueString.toString().padStart(2, '0');
};
