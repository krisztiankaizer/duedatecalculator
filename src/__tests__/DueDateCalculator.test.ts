import DueDateCalculator from '../DueDateCalculator';

let dueDateCalculator: DueDateCalculator;

describe('Calculator', () => {
  beforeAll(() => {
    dueDateCalculator = new DueDateCalculator();
  });

  describe('calculateDueDate', () => {
    test('should return the same date if 0 necessaryHours provided', () => {
      // 2018-05-24 - thursday
      const startDateISOString = '2018-05-24 09:00:00';
      const expectedISODateString = '2018-05-24 09:00:00';
      const necessaryHours = 0;

      const result = dueDateCalculator.calculateDueDate(startDateISOString, necessaryHours);
      expect(result).toEqual(expectedISODateString);
    });

    test('should work with `necessaryHours` when the expected due date within a day', () => {
      // 2018-05-24 - thursday
      const startDateISOString = '2018-05-24 09:00:00';
      const expectedISODateString = '2018-05-24 13:00:00';
      const necessaryHours = 4;

      const result = dueDateCalculator.calculateDueDate(startDateISOString, necessaryHours);
      expect(result).toEqual(expectedISODateString);
    });

    test('should work when the expected due date ended at the end of the day', () => {
      // 2018-05-25 - friday
      const startDateISOString = '2018-05-25 16:00:00';
      const expectedISODateString = '2018-05-25 17:00:00';
      const necessaryHours = 1;

      const result = dueDateCalculator.calculateDueDate(startDateISOString, necessaryHours);
      expect(result).toEqual(expectedISODateString);
    });

    test('should work when the expected due date not ended in start day (tomorrow)', () => {
      // 2018-05-24 - thursday
      const startDateISOString = '2018-05-24 16:00:00';
      const expectedISODateString = '2018-05-25 12:00:00';
      const necessaryHours = 4;

      const result = dueDateCalculator.calculateDueDate(startDateISOString, necessaryHours);
      expect(result).toEqual(expectedISODateString);
    });

    // tslint:disable-next-line
    test('should work when the expected due date not ended in start day (the day after tomorrow)', () => {
      // 2018-05-24 - wednesday
      const startDateISOString = '2018-05-23 16:00:00';
      const expectedISODateString = '2018-05-25 12:00:00';
      const necessaryHours = 12;

      const result = dueDateCalculator.calculateDueDate(startDateISOString, necessaryHours);
      expect(result).toEqual(expectedISODateString);
    });

    test('should work when the expected due date falls after a weekend', () => {
      // 2018-05-24 - thursday
      const startDateISOString = '2018-05-24 16:00:00';
      const expectedISODateString = '2018-05-28 12:00:00';
      const necessaryHours = 12;

      const result = dueDateCalculator.calculateDueDate(startDateISOString, necessaryHours);
      expect(result).toEqual(expectedISODateString);
    });

    test('should work when the expected due date falls after more than one weekend', () => {
      // 2018-05-24 - thursday
      const startDateISOString = '2018-05-24 16:00:00';
      const expectedISODateString = '2018-06-04 12:00:00';
      const necessaryHours = 52;

      const result = dueDateCalculator.calculateDueDate(startDateISOString, necessaryHours);
      expect(result).toEqual(expectedISODateString);
    });

    test('should throw error if reported date falls not a workday', () => {
      expect(() => {
        // 2018-05-26 - saturday
        const startDateISOString = '2018-05-26 16:00:00';
        const necessaryHours = 52;
        dueDateCalculator.calculateDueDate(startDateISOString, necessaryHours);
      }).toThrow();
    });

    test('should throw error if not valid date added', () => {
      expect(() => {
        const startDateISOString = 'not a valid date';
        const necessaryHours = 52;
        dueDateCalculator.calculateDueDate(startDateISOString, necessaryHours);
      }).toThrow();
    });

    test('should work when the task arrived in last minute at the end of the day', () => {
      // 2018-05-24 - thursday
      const startDateISOString = '2018-05-24 17:00:00';
      const expectedISODateString = '2018-05-25 10:00:00';
      const necessaryHours = 1;

      const result = dueDateCalculator.calculateDueDate(startDateISOString, necessaryHours);
      expect(result).toEqual(expectedISODateString);
    });

    test('should work when the ticket arrives at not complete hours', () => {
      // 2018-05-24 - thursday
      const startDateISOString = '2018-05-24 16:30:00';
      const expectedISODateString = '2018-05-25 09:30:00';
      const necessaryHours = 1;

      const result = dueDateCalculator.calculateDueDate(startDateISOString, necessaryHours);
      expect(result).toEqual(expectedISODateString);
    });

    test('should work when the ticket arrives at not complete hours', () => {
      // 2018-05-24 - thursday
      const startDateISOString = '2018-05-24 16:45:00';
      const expectedISODateString = '2018-05-25 09:45:00';
      const necessaryHours = 1;

      const result = dueDateCalculator.calculateDueDate(startDateISOString, necessaryHours);
      expect(result).toEqual(expectedISODateString);
    });
  });
});
