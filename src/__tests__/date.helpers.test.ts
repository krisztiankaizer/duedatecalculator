
import { transformToTwoDigits } from '../date.helpers';

describe('date.helpers', () => {
  describe('transformToTwoDigits', () => {
    test('should transform to 2 digits', () => {
      expect(transformToTwoDigits(1)).toBe('01');
      expect(transformToTwoDigits('1')).toBe('01');
      expect(transformToTwoDigits(10)).toBe('10');
      expect(transformToTwoDigits('100')).toBe('100');
    });
  });
});
