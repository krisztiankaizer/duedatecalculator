
import  { Days } from './date.helpers';

const MILLISECONDS_IN_A_HOUR = 60 * 60 * 1000;

Date.workdaysStartAt = 9;
Date.workdaysEndAt = 17;
Date.workdays = [Days.MONDAY, Days.TUESDAY, Days.WEDNESDAY, Days.THURSDAY, Days.FRIDAY];

Date.prototype.addHours = function (hours: number): Date {
  this.setHours(this.getHours() + hours);
  return this;
};

Date.prototype.addDays = function (days: number): Date {
  this.setDate(this.getDate() + days);
  return this;
};

Date.prototype.getHoursUntilWorkdayEnds = function (): number {
  const time: number = this.getTime();

  const endDate: Date = new Date(time);
  endDate.setHours(Date.workdaysEndAt);
  endDate.setMinutes(0);

  const diff: number = endDate.getTime() - time;
  const diffInHours: number = diff / MILLISECONDS_IN_A_HOUR;

  return diffInHours;
};

Date.prototype.isValidCheck = function (): boolean {
  return Date.workdays.includes(this.getDay());
};

Date.prototype.isWorkday = function (): boolean {
  return Date.workdays.includes(this.getDay());
};

Date.prototype.addDayUntilNextWorkday = function (): Date {
  this.addDays(1);
  while (!this.isWorkday()) {
    this.addDays(1);
  }
  return this;
};
