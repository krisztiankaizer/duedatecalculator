interface DateConstructor {
  workdaysStartAt: number;
  workdaysEndAt: number;
  workdays: number[];
}
interface Date {
  addHours(hours: number): Date;
  addDays(days: number): Date;
  isWorkday(): boolean;
  isValidCheck(): boolean;
  getHoursUntilWorkdayEnds(): number;
  addDayUntilNextWorkday(): Date;
}
