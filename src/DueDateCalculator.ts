import './date.extensions.ts';
import { Days, transformToTwoDigits } from './date.helpers';

interface DueDateCalculatorOptions {
  workdaysStartAt: number;
  workdaysEndAt: number;
  workdays: number[];
}
class DueDateCalculator {
  private settings: DueDateCalculatorOptions;
  private workingHoursInAday: number;
  private defaultOptions: DueDateCalculatorOptions = {
    workdaysStartAt: 9,
    workdaysEndAt: 17,
    workdays: [Days.MONDAY, Days.TUESDAY, Days.WEDNESDAY, Days.THURSDAY, Days.FRIDAY],
  };

  constructor (options?: DueDateCalculatorOptions) {
    this.settings = Object.assign({}, this.defaultOptions, options);
    this.workingHoursInAday = this.settings.workdaysEndAt - this.settings.workdaysStartAt;

    Date.workdaysStartAt = this.settings.workdaysStartAt;
    Date.workdaysEndAt = this.settings.workdaysEndAt;
    Date.workdays = this.settings.workdays;
  }

  private formatDate (date: Date) {
    const year = date.getFullYear();
    const month = transformToTwoDigits(date.getMonth() + 1);
    const day = transformToTwoDigits(date.getDate());
    const hours = transformToTwoDigits(date.getHours());
    const minutes = transformToTwoDigits(date.getMinutes());
    const seconds = transformToTwoDigits(date.getSeconds());

    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
  }

  calculateDueDate (startDateString: string, necessaryHours: number): string {
    const date = new Date(startDateString);

    if (!date.isValidCheck() || !date.isWorkday()) {
      throw new Error('The date is probably not a valid string or the date doesn`t fall workday.');
    }

    let remainingHours: number = necessaryHours - date.getHoursUntilWorkdayEnds();
    const willEndSameDay = remainingHours <= 0;

    if (willEndSameDay) {
      remainingHours = necessaryHours;
    } else {
      date.addDayUntilNextWorkday();
      while (remainingHours > this.workingHoursInAday) {
        remainingHours -= this.workingHoursInAday;
        date.addDayUntilNextWorkday();
      }

      const startAt = this.settings.workdaysStartAt;
      date.setHours(startAt);
    }

    const result = date.addHours(remainingHours);
    return this.formatDate(result);
  }
}

export default DueDateCalculator;
